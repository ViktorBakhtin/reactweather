import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import WrappedMainComponent from './App';
import * as serviceWorker from './serviceWorker';
import {createStore} from "redux";
import {connect, Provider} from 'react-redux'

const initialState = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    error: "",
    array_city: JSON.parse(localStorage.getItem('cities')),
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SHOW_CITY":
            return {
                temperature: action.payload.temperature,
                city: action.payload.city,
                country: action.payload.country
            };
            break;
        case 'ERROR_MESSAGE':
            return {
                error: "Enter a valid city name"
            };
            break;
        case 'SET_ARRAY':
            return {
                ...state,
                array_city: action.payload
            };
    }
    return state;
};

const store = createStore(rootReducer);


ReactDOM.render(<Provider store={store}>
    <WrappedMainComponent/>
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
