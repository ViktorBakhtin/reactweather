import React from "react"

const ListCity = props => {
    if(props.cities !=null && props.cities.length !==0){
        return(
            <div className="borderMarginPadding">
                <div>{props.cities.map(city => (
                    <div>
                        <p>{city.city}: {city.temperature}
                        <a  style={{fontSize: '13px', color: "grey", cursor: 'pointer'}} onClick={()=>props.deleteCity(city.city)}>|Remove|</a>
                        <a  style={{fontSize: '13px', color: "grey", cursor: 'pointer'}} onClick={()=>props.getWeather(city.city,"update")}>Update</a>
                        </p>
                    </div>
                ))}
                </div>
                <button onClick={()=>props.getWeather(props.cities,"update_all")}>Update all cities</button>
            </div>
        )
    }else {
        return(
           <div></div>
        )
    }
}
export default ListCity
