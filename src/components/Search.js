import React from "react";
import Autocomplete from 'react-google-autocomplete';

const Search = props => (
            <Autocomplete className="borderMarginPadding"
                style={{width: '90%'}}
                onPlaceSelected={(place) => {
                    props.getWeather(place.name, "current_city")
                }}
                types={['(regions)']}
                // componentRestrictions={{}} //Select country only search
            />
        )
export default Search;

