import React from "react"


const CurrentCityWeather = props => (
    <div className="borderMarginPadding">
        {props.temperature && <p> Temperature: {props.temperature}</p>}
        {props.city && <p> City: {props.city}</p>}
        {props.country && <p> Country: {props.country}</p>}
        {props.error && <p> Error: {props.error}</p>}
        {props.city && <button onClick={()=>props.addListCity({city: props.city})}>Add to list</button>}
    </div>
)

export default CurrentCityWeather
