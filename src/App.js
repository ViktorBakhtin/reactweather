import React, {Component} from "react"
import HeaderSum from "./components/HeaderSum"
import Search from "./components/Search"
import CurrentCityWeather from "./components/CurrentCityWeather.js"
import ListCity from "./components/ListCity";
import {connect} from "react-redux";

const API_KEY = "2bd83f762b46b0e103cc437a5a14b3a7"; //Weather
//AIzaSyAmzgRfMoLpxHz3SdLrWnYsjdyUH3Ud9aw           //Google index.html script
const maxCount = 5;

class App extends Component {
    weatherCallApi = async (name_city) => {
        return await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${name_city}&APPID=${API_KEY}&units=metric`);
    };

    getWeather = async (name_city, type) => {
        let api_call = "";
        let data = "";
        switch (type) {
            case "current_city":
                api_call = await this.weatherCallApi(name_city);
                data = await api_call.json();
                if (data.name && data.sys.country) {
                    const showCity = () => {
                        return {
                            type: "SHOW_CITY",
                            payload: {
                                temperature: data.main.temp,
                                city: data.name,
                                country: data.sys.country,
                            }
                        }
                    };
                    this.props.dispatch(showCity());
                } else {
                    const throwError = () => {
                        return {
                            type: "ERROR_MESSAGE",
                            payload: {
                                error: "Enter a valid city name"
                            }
                        }
                    };
                    this.props.dispatch(throwError());
                }
                break;
            case "update":
                api_call = await this.weatherCallApi(name_city);
                data = await api_call.json();
                let array = JSON.parse(localStorage.getItem('cities'));
                let arr = array.filter(function (element) {
                    if (element.city === name_city) {
                        element.temperature = data.main.temp
                    }
                    return element
                });
                this.setArrayToLocalStorage(arr);
                break;
            case "update_all":
                for (let i = 0; i < name_city.length; i++) {
                    api_call = await this.weatherCallApi(name_city[i].city);
                    data = await api_call.json();
                    name_city[i].temperature = data.main.temp
                }
                this.setArrayToLocalStorage(name_city);
                break;
            default:
        }
    };

    headerSummuryTemp = () => {
        let storage = localStorage.getItem('cities');
        var array = JSON.parse(storage);
        if (storage !== null && array.length > 0) {
            var total = 0;
            var filtered = array.filter(element => {
                return element.temperature
            });
            if (filtered.length > 0) {
                for (var i = 0; i < filtered.length; i++) {
                    total += filtered[i].temperature;
                }
                var avg = total / filtered.length;
                return avg.toFixed(1)
            }
            return false

        } else {
            return false
        }
    };

    addListCity = (obj) => {
        let storage = localStorage.getItem('cities');
        let array = [];
        if (storage !== null) {
            if (storage.search(obj.city) >= 0) {
                return alert("You already add this city")
            }
            array = JSON.parse(storage);
        }

        if (array.length < maxCount) {
            array.push(obj);
            const addCity = () => {
                return {
                    type: "SET_ARRAY",
                    payload: array
                }
            };
            this.props.dispatch(addCity());
            let object = JSON.stringify(array);
            localStorage.setItem('cities', object);
        } else {
            alert("You can not add more cities(max: 5)")
        }
    };

    deleteCity = (name_city) => {
        let array = JSON.parse(localStorage.getItem('cities'));
        let arr = array.filter(function (element) {
            if (element.city !== name_city) {
                return element
            }
        });
        this.setArrayToLocalStorage(arr)
    };

    setArrayToLocalStorage = (arr) => {
        let object = JSON.stringify(arr);
        localStorage.setItem('cities', object);

        const setArray = () => {
            return {
                type: "SET_ARRAY",
                payload: arr
            }
        };
        this.props.dispatch(setArray());
    };

    render() {
        return (
            <div>
                <HeaderSum headerSummuryTemp={this.headerSummuryTemp} show={this.headerSummuryTemp()}/>
                <Search getWeather={this.getWeather}/>
                <CurrentCityWeather
                    addListCity={this.addListCity}
                    temperature={this.props.temperature}
                    city={this.props.city}
                    country={this.props.country}
                    error={this.props.error}
                />
                <ListCity cities={this.props.array_city}
                          deleteCity={this.deleteCity}
                          getWeather={this.getWeather}
                />
            </div>
        );
    }
}

const mapStatetoProps = (state) => {
    return {
        temperature: state.temperature,
        city: state.city,
        country: state.country,
        error: state.error,
        array_city: JSON.parse(localStorage.getItem('cities'))
    }
}

const WrappedMainComponent = connect(mapStatetoProps)(App);
export default WrappedMainComponent;
